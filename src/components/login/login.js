import io from 'socket.io-client';

export default {
  name: 'login',
  components: {},
  props: [],
  data() {
    return {
      playerNbr: 2,
      username: 'Player',
      playersCount: 0,
      lobbyState: 'logging', // logging, waiting, playing
      socket: io('http://192.168.1.7:3000'),
      allPlayersReady: false,
      gameCountDown: null,
      startingCountDown: null,
      playerWon: null,
      rightAnswerCard: false,
      wrongAnswerCard: false,
      playerWonCard: false,
      gameOverCard: false,
      hintMsg: 'Nothing',
      hints: [
        { distance: 10, msg: 'You are very close (Interval of +-10' },
        { distance: 25, msg: 'You are far (Interval of +-25)' },
        { distance: 100, msg: 'You are so far, i give up on you' },
      ],
      cardImages: {
        wrongGuess: '116258.png',
        rightGuess: '116255.png',
        playerWon: '116266.png',
        gameOver: '116276.png',
      },
    };
  },
  computed: {

  },
  mounted() {
    this.socket.on('lobbyInfo', (data) => {
      if (data.state === 200) {
        this.playersCount = data.playersCount;
        this.lobbyState = 'waiting';
        // console.log(this.playersCount);
      } else {
        // alert('You cant join the room');
      }
    });

    this.socket.on('joinFailed', (data) => {
      if (data.state === 300) {
        this.$dialog.alert('You can\'t join, the room is full of the game have already started.');
      }
    });

    this.socket.on('playersCount', (playersCount) => {
      this.playersCount = playersCount;
      if (this.playersCount === 2) {
        // pass
      }
    });

    this.socket.on('gameStart', () => {
      this.allPlayersReady = true;
      let startingDuration = 4;
      this.startingCountDown = setInterval(() => {
        startingDuration -= 1;
        if (this.playersCount < this.playerNbr) {
          this.allPlayersReady = false;
          clearInterval(this.startingCountDown);
        } else {
          document.getElementById('startingCountDown').innerText = `Game starting in: ${startingDuration}`;
        }
        if (startingDuration < 0) {
          this.allPlayersReady = false;
          clearInterval(this.startingCountDown);
          this.startGame();
        }
      }, 1000);
    });

    this.socket.on('wrongGuess', (data) => {
      // console.log(`${index} is a wrong guess`);
      this.hintMsg = this.guessHintMsg(data.distance);
      this.wrongAnswerCard = true;
      /*
      this.informativeCard(
        this.cardImages.wrongGuess,
        'Wrong Answer!!!',
        'Hint: Nothing',
      );
      */
      this.$refs[`guess${data.index}`][0].classList = 'column button is-danger btnSize is-3-mobile is-2-tablet is-1-desktop disabledDiv';
    });

    this.socket.on('playerWin', (username) => {
      clearInterval(this.gameCountDown);
      if (this.username === username) {
        // alert(`Conglaturation ${username}, you won the game`);
        this.rightAnswerCard = true;
        /*
        this.informativeCard(
          this.cardImages.rightGuess,
          `Conglaturation ${this.username}, You Won!!!`,
          null,
        );
        */
      } else {
        // alert(`You lost, ${username} have won the game`);
        this.playerWon = username;
        this.playerWonCard = true;
      }
      document.getElementById('guessCanvas').classList = 'columns is-mobile is-multiline disabledDiv';
      setTimeout(() => {
        this.socket.emit('gameEnded');
      }, 3000);
    });

    this.socket.on('gameOver', () => {
      // this.username = '';
      this.playersCount = 0;
      this.lobbyState = 'logging';
      this.gameCountDown = null;
      this.startingCountDown = null;
      this.hintMsg = 'Nothing';
      this.playerWon = null;
      this.rightAnswerCard = false;
      this.wrongAnswerCard = false;
      this.playerWonCard = false;
      this.gameOverCard = false;
    });
  },
  methods: {
    joinBtn() {
      this.socket.emit('joining', this.username);
    },
    readyBtn() {
      document.getElementById('readyBtn').classList = 'disabledDiv';
      this.socket.emit('ready');
    },
    startGame() {
      this.lobbyState = 'playing';
      let gameDuration = 30;
      this.gameCountDown = setInterval(() => {
        gameDuration -= 1;
        document.getElementById('counter').innerText = `${gameDuration} sec`;
        if (gameDuration < 0) {
          clearInterval(this.gameCountDown);
          this.gameOverCard = true;
          document.getElementById('counter').innerText = 'GAME OVER';
          document.getElementById('guessCanvas').classList = 'columns is-mobile is-multiline disabledDiv';
          setTimeout(() => {
            this.socket.emit('gameEnded');
          }, 3000);
        }
      }, 1000);
    },
    sendGuess(index) {
      // console.log(`clicked ${index}`);
      this.socket.emit('guess', {
        guess: index,
        username: this.username,
      });
    },
    guessHintMsg(distance) {
      for (let i = 0; i < this.hints.length; i += 1) {
        if (this.hints[i].distance >= distance) {
          return this.hints[i].msg;
        }
      }
      return 'Sorry, no hit for you';
    },
    /*
    informativeCard(imageName, cardTitle, cardContent) {
      this.$modal.open(
        `<b-modal :active.sync="rightAnswerCard" width="320">
          <div class="card">
            <div class="card-image">
              <figure class="image">
                <img src="../../../static/stickers/116257.png">
              </figure>
            </div>
            <div class="card-content">
              <div class="title has-text-centered">
                ${cardTitle}
              </div>
              <div class="subtitle">
                ${cardContent}
              </div>
            </div>
          </div>
        </b-modal>`,
      );
    },
    */
  },
};
